# Predicting house prices in King County, WA, USA

Exploratory data analysis for the house prices in King County, WA, USA using the kc_house_dataset

## Packages required

Common packages for EDA : `pandas`, `numpy`, `seaborn`, `statsmodels.api`, `sklearn`, `matplotlib`

## Dataset Info

This dataset contains house sale prices for King County, which includes Seattle. It was retrieved
from https://www.kaggle.com/harlfoxem/housesalesprediction. It was published on 2016-08-25. The 
dataset is famous and features are complete so it is considered reliable.

Along with house price (target) it consists of an ID, date, and 18 house features.


1. `Id`:  Unique ID for each home sold
2. `Date`: Date of the home sale
3. `Price`: Price of each home sold (target)
4. `Bedrooms`: Number of bedrooms
5. `Bathrooms`: Number of bathrooms, where .5 accounts for a room with a toilet but no shower
6. `Sqft_living`: Square footage of the apartments interior living space
7. `Sqft_lot`: Square footage of the land space
8. `Floors`: Number of floors
9. `Waterfront`: A dummy variable for whether the apartment was overlooking the waterfront or not
10. `View`: An index from 0 to 4 of how good the view of the property was
11. `Condition`: An index from 1 to 5 on the condition of the apartment,
12. `Grade`: An index from 1 to 13, where 1-3 falls short of building construction and design, 7 has an average level of construction and design, and 11-13 have a high quality level of construction and design
13. `Sqft_above`: The square footage of the interior housing space that is above ground level
14. `Sqft_basement`: The square footage of the interior housing space that is below ground level
15. `Yr_built`: The year the house was initially built
16. `Yr_renovated`: The year of the house’s last renovation
17. `Zipcode`: What zipcode area the house is in
18. `Lat`: Lattitude
19. `Long`: Longitude
20. `Sqft_living15`: The square footage of interior housing living space for the nearest 15 neighbors
21. `Sqft_lot15`: The square footage of the land lots of the nearest 15 neighbors

## Conclusions

->TO DO
